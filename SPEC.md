# Specification

- Title: Kubernetes Pod Garbage Collation
- Author(s): Steve Azzopardi
- Team: ~"group::runner"
- Reviewer(s): N/A
- Created on: 2021-04-10
- Last updated: 2021-04-10
- Issue reference: https://gitlab.com/gitlab-org/gitlab-runner/-/issues/27702

## Introduction

### Overview

Delete Kubernetes Pods that are no longer used to stop using resources, and only
have Pods that are necessary.

### Glossary

- **Kubernetes/k8s**: https://kubernetes.io/
- **Pod**: https://kubernetes.io/docs/concepts/workloads/pods/
- **Expired Pods**: Pods that where created but never cleaned up and no longer provide value.
- **Pod Annotation**: https://kubernetes.io/docs/concepts/overview/working-with-objects/annotations/
- **Garbage Collection/GC**: The process of removing expired Pods.
- **RBAC**: https://kubernetes.io/docs/reference/access-authn-authz/rbac/
- **CronJob/Cron**: https://kubernetes.io/docs/concepts/workloads/controllers/cron-jobs/
- **podgc**: The name of this project.
- **Go**: http://golang.org/

### Background

As part of https://gitlab.com/gitlab-org/gitlab-runner/-/issues/27702
, [GitLab Runner](https://gitlab.com/gitlab-org/gitlab-runner) creates Pods to
run CI Jobs inside them, as
[identified](https://gitlab.com/gitlab-org/gitlab-runner/-/issues/27719#note_547496279)
it can be the case that it can't delete a Pod and just leaves it around. GitLab
Runner should delete Pods on the best effort basis but can't do much if it's not
allowed to do so (terminated, network problems).

### Goal

- Least privileged should only have list/delete on the `pods` resource.
- Compliant with OpenShift.
- Running rootless container.
- Concurrent safe.
- Easy to delay.
- Audit log of what was deleted and why.
- The user decides which pods are deleted.
- CLI tool that can run inside a Cluster or users laptop.
- Support supported versions of Kubernetes.

### Non-Goals

- Automatically deciding if a pod is expired or not, the user decides this.

### Future Goals

- Add `deploy` command to deploy a CronJob.
- Multiple `podgc` can run at the same time.

## Solution

### Design

#### Business Logic

Have a CLI command called `podgc` written in Go that will interact with
the [Kubernetes API](https://kubernetes.io/docs/concepts/overview/kubernetes-api/)
using [client-go](https://github.com/kubernetes/client-go). `podgc` will get all
the pods available in the cluster in all allowed namespaces.

All the Pods that want to get garbage collected need to have
`podgc.gitlab.com/ttl` Pod Annotation. The value will be an integer representing
in seconds how long the Pod should be alive for. For example
`podgc.gitlab.com/ttl: 3600` means that the Pod can be deleted after it's been
created for 1 hour.

When `podgc` finds a pod that is expired with will send a [delete
Pod](https://kubernetes.io/docs/reference/kubernetes-api/workload-resources/pod-v1/#delete-delete-a-pod)
request, when multiple expired pods are found it will send the delete request
sequentially for the initial iteration.

```mermaid
sequenceDiagram
    podgc->>+kubernetes: GET /api/v1/pods
    kubernetes->>-podgc: list objects of kind Pod
    Note right of podgc: Calculate expired pods
    podgc->>+kubernetes: DELETE /api/v1/namespaces/{namespace}/pods/{name}
```

### Audit log

`podgc` will use JSON [structured
logs](https://stackify.com/what-is-structured-logging-and-why-developers-need-it/)
as an audit trail for the Pods are marked as expired, send a DELETE request and
got a valid DELETE response.

Each log will consist of the following information:

- `version`: The version of `podgc`.
- `pid`: The PID of the process.
- `timestamp`: A timestamp in UTC.
- `pod`: JSON object with Pod information
    - `name`: The name of the pod.
    -`namespace`: The namespace of the pod.
    -`created_at`: The creation time of the pod.
    -`podgc.gitlab.com/tll`: The value of the Pod annotation

Log levels

- `INFO`: Logging events happening in `podgc` for audit trail.
- `ERROR`: Failures in the application that the user needs to take action on.

### Test Plan

#### Unit Tests

Unit tests will run the test the logic on a unit level, mocking the Kubernetes
API to control the requests/response. These tests run on every commit through
continuous integration.

#### E2E Tests

End-to-end tests that will spin up a Kubernetes cluster create a ton of Pods
some of them Expired pods. Run `podgc` and validate that only the expired pods
are deleted. These tests will run on every commit through continuous integration.

### Safety mechanisms

`podgc` is going to have the ability to delete resources, and if miss-configured
or a bug exists in the code base can cause many Pods that might be running
production applications to be deleted. This is why `podgc` can only delete 10
pods per run to prevent any harm and also allow users to react if it's
misbehaving. There is still a flaw with this plan, where there can be a bug in
the limiter, but this will reduce the risk.

### Security

The Kubernetes administrator can choose if this will delete expired Pods for all
namespaces or for a specific configuration. The Kubernetes administrator should
relay on [Kubernetes RBAC](https://kubernetes.io/docs/reference/access-authn-authz/rbac/) to limit
the permissions of `podgc`.
